// Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This test is basically that linking against libcrash doesn't alone
// cause a test to fail.  It's OK if it causes a syslog message
// though.

int main() {
  return 0;
}
