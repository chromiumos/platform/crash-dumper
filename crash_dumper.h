// Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CRASH_CRASH_DUMPER_H_
#define CRASH_CRASH_DUMPER_H_

// Class to manage crash handling and dumping.  When Enable is called, all
// crashes will be caught and stored to the appropriate crash directory.
// The directory will be:
//   /home/chronos/user/crash - for all processes running as chronos
//   /var/spool/crash - for all other processes
// The class takes care of creating the directories (even recreating them
// at crash time in case the cryptohome mounting changes from Enable time.
//
// For most use cases, there is no need to include or call any functions
// explicitly in this header file.  Crash dumping is enabled just by linking
// in libcrash_dumper.

class CrashDumper {
 public:
  CrashDumper() {
    Enable();
  }

  ~CrashDumper() {
    if (IsEnabled()) {
      Disable();
    }
  }

  // Enable crash detection and dumping.  Aborts if already enabled
  // or crash reporting cannot be enabled.  If the cryptohome is mounted
  // while crash handling is enabled, later crashes may be lost.
  static void Enable();

  // Return if enabled.
  static bool IsEnabled();

  // Disable crash detection and dumping.  Aborts if not enabled.
  static void Disable();
};

#endif  // CRASH_CRASH_DUMPER_H_
