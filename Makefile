# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

CRASH_LIB = libcrash.so

INCLUDE_DIRS = -I.. -I$(SYSROOT)/usr/include/google-breakpad

# We need -fPIC for linking objects into shared objects.
CXXFLAGS += -fPIC -Wall -Werror

all: $(CRASH_LIB)

tests: innocuous_host_lcrash_test

$(CRASH_LIB): crash_dumper.o
	$(CXX) -shared -lbreakpad_client $^ -o $@

innocuous_host_lcrash_test: innocuous_host_lcrash_test.o $(CRASH_LIB)
	$(CXX) $^ -o $@ ${CRASH_LIB}

.cc.o:
	$(CXX) $(CXXFLAGS) $(INCLUDE_DIRS) -c $< -o $@

clean:
	rm -rf *.o $(CRASH_LIB)
